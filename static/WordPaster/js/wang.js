window.wangEditor.wordpaster = {
    init: function(editor,w) {    	
    	$(editor).find(" .w-e-toolbar").append("<div class='w-e-menu wordpaster' title='Word一键粘贴' onclick='window.wangEditor.wordpaster.run()'></div>");
    },
	run: function(){
        WordPaster.getInstance().Paste();
	}
};
window.wangEditor.pptimport = {
    init: function (editor, w) {
        $(editor).find(" .w-e-toolbar").append("<div class='w-e-menu pptimport' title='PowerPoint一键导入' onclick='window.wangEditor.pptimport.run()'></div>");
    },
    run: function () {
        WordPaster.getInstance().importPPT();
    }
};