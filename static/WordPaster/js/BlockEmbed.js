import { Quill } from 'vue-quill-editor';
// Import the BlockEmbed blot.
var BlockEmbed = Quill.import('blots/block/embed');

// Create a new format based off the BlockEmbed.
class Footer extends BlockEmbed {

    // Handle the creation of the new Footer format.
    // The value will be the HTML that is embedded.
    // This time the value is passed from our custom handler.
    static create(value) {
        // Create the node using the BlockEmbed's create method.
        var node = super.create("p");
        node.innerHTML = value;
        // Set the srcdoc attribute to equal the value which will be your html.
        //node.setAttribute('srcdoc', value);

        // Add a few other iframe fixes.
        //node.setAttribute('frameborder', '0');
        //node.setAttribute('allowfullscreen', true);
        //node.setAttribute('width', '100%');
        return node;
    }

    // return the srcdoc attribute to represent the Footer's value in quill.
    static value(node) {
        return node;
    }

}

// Give our new Footer format a name to use in the toolbar.
Footer.blotName = 'footer';

// Give it a class name to edit the css.
//Footer.className = 'ql-footer';

// Give it a tagName of iframe to tell quill what kind of element to create.
Footer.tagName = 'div';

// Register the new Footer format so we can use it in our editor.
Quill.register(Footer, true);

// var quill = new Quill('#editor-container', {
//     modules: {
//         toolbar: {
//             container: ['footer'] // Toolbar with just our footer tool (of course you can add all you want).
//         }
//     },
//     theme: 'snow'
// });
